Dialogue <- {}

Dialogue.text <- TextObject();
Dialogue.text.set_wrap_width(sector.Camera.get_screen_width()/3 - 32);
Dialogue.text.set_pos(0, -64);
Dialogue.text.set_centered(true);
Dialogue.text.set_text("kec a kec a kec");
Dialogue.text.set_anchor_point(ANCHOR_BOTTOM);
Dialogue.text.set_visible(false);

Dialogue.text_skip <- TextObject();
Dialogue.text_skip.set_wrap_width(sector.Camera.get_screen_width()/3 - 32);
Dialogue.text_skip.set_pos(0, -24);
Dialogue.text_skip.set_centered(true);
Dialogue.text_skip.set_text(_("Press ACTION to continue."));
Dialogue.text_skip.set_anchor_point(ANCHOR_BOTTOM);
Dialogue.text_skip.set_back_fill_color(0,0,0,0);
Dialogue.text_skip.set_front_fill_color(0,0,0,0);
//Dialogue.text_skip.set_roundness(0);
//Dialogue.text_skip.set_text_color(0,0.7,1,1);
Dialogue.text_skip.set_visible(false);

Dialogue.character <- function(sprite, name, right) {
	ret <- {}
	ret.face <- FloatingImage("levels/tuxquest/images/characters/"+sprite+"_face.png");
	ret.face.set_layer(1000);
	ret.face.set_anchor_point(ANCHOR_BOTTOM);
	if (right) {
		ret.face.set_pos(sector.Camera.get_screen_width()/6+128, 0);
	} else {
		ret.face.set_pos(-sector.Camera.get_screen_width()/6-128, 0);
	}
	ret.face.set_visible(false);
	ret.name <- name;
	return ret
}

//test <- Dialogue.character("tux", "Tux")
//test2 <- Dialogue.character("alpha_sierra", "Tux")
//test.face.set_visible(true);

Dialogue.run <- function(replies) {
	Dialogue.text.set_visible(true);
	Dialogue.text_skip.set_visible(true);
	foreach (reply in replies) {
		reply[0].face.set_visible(true);
		Dialogue.text.set_text(reply[0].name + ": " + _(reply[1]));
		//wait(5);
		while(!sector.Tux.get_input_held("action")) {
			if(sector.Tux.get_input_held("escape")) {
				reply[0].face.set_visible(false);
				Dialogue.text.set_visible(false);
				Dialogue.text_skip.set_visible(false);
				return;
			}
			wait(0);
		}
		while(sector.Tux.get_input_held("action")) 
			wait(0);
		reply[0].face.set_visible(false);
	}
	Dialogue.text.set_visible(false);
	Dialogue.text_skip.set_visible(false);
}

